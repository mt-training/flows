## 3. Create a Template Attributes

In this exercise, you will create two attributes for the gaming products template Flow.

### Add Play Mode Attribute to the Template Flow

In this step, you will create an attribute `play_mode` to store the play mode (eg, mlutiplayer or singleplayer) for your store's gaming products.

* Open the `Create a field` request from `flows` folder.
* Replace the contents in the body section with::

```json
{
    "data": {
        "type": "field",
        "name": "Play Mode",
        "slug": "play_mode",
        "field_type": "string",
        "validation_rules": [
            {
                "type": "enum",
                "options": [
                    "free",
                    "multiplayer",
                    "singleplayer"
                ]
            }
        ],
        "description": "Game play mode",
        "required": false,
        "enabled": true,
        "relationships": {
            "flow": {
                "data": {
                    "type": "flow",
                    "id": "{{flowID}}"
                }
            }
        }
    }
}
```

* Click `Send`.
* The script in the Tests section of the request stores the Field ID returned in a variable called `fieldID`.

### Add Age Rating Attribute to the Template Flow

In this step, you will create an attribute `age_rating` to store the age rating for your store's gaming products.

* Open the `Create a field` request from `flows` folder.
* Replace the contents in the body section with::

```json
{
    "data": {
        "type": "field",
        "name": "Age Rating",
        "slug": "age_rating",
        "field_type": "string",
        "validation_rules": [
            {
                "type": "enum",
                "options": [
                    "E for Everyone",
                    "Everyone 10+",
                    "T for Teen",
                    "M for Mature"
                ]
            }
        ],
        "description": "Game age rating",
        "required": false,
        "enabled": true,
        "relationships": {
            "flow": {
                "data": {
                    "type": "flow",
                    "id": "{{flowID}}"
                }
            }
        }
    }
}
```

* Click `Send`.
* The script in the Tests section of the request replaces the Field ID in the `fieldID` variable with the value from this new attribute.

### Get the Gaming Template Attributes

You've created a template with two attributes. Send a GET request to get the template and all attributes on it to confirm. 

* In Postman, open the `Get all fields on a flow` request from `flows` folder.
* Select `Send`.

[Next: 4. Create Product-Template Relationship](./product_template_relationship.md)