## 4. Create a Product-Template Relationship

In this exercise, you will apply the gaming template to one of your gaming products through a Product-Template Relationship.

### Create Product-Template Relationship

* Open a new blank POST request in Postman. 
* Enter the following in the URL field: `{{pcmUrl}}/products/{{productID}}/relationships/templates`
* Confirm the `{{productID}}` environment variable value is that of your PCM Training Product from Step 1. 
>Note: Environment variables are case sensitive. Ensure the product ID value is saved in a variable called `{{productID}}` and not `{{productId}}`.
* Paste the following into the body section:

```json
{
    "data": [{
        "type": "template",
        "id": "{{flowID}}"
    }]
}
```

* Complete the Authorization tab with the following information:
    Type: Bearer
    Token: {{accessToken}}
* Click `Send`.

[Next: 5. Create Entry](./template-flow-entries.md)