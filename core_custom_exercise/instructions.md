## Create a Wishlist with Core and Custom Flows

In these exercises, you will use flows to extend the data schema to store wishlists and extend the customer resource to associate it with the wishlist resource.

### Objectives

* Create a Flow for your wishlist object.
* Create products Field to store customer's desired products.
* Create empty Entries to associate with products.
* Create a relationship between an Entry and a product. This will be created every time a customer adds a product to a wishlist.
* Associate a wishlist with a customer by adding a customers Flow and creating a one-to-many relationship between a customer and a wishlist.

### Exercises

1. [Create a custom resource using Flows](./custom-resource.md)
2. [Create entries in a custom Flow](./custom-flow-entries.md)
3. [Extend a core resource](./core-resource.md)
4. [Create entries in a core Flow](./core-flow-entries.md)
