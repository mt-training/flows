## Create an Empty Wishlist (Flow Entry)

With the custom Flow configured, next you will create an empty wishlist Entry and then add product IDs to it.
Since the only field in the flow is a relationship field, we will be creating an empty entry.

### Create an Empty Wishlist (Flow Entry)

* In Postman, open the `Create an entry` request from `flows` folder.
* The `POST` request URL is `{{baseUrl}}/flows/{{flowSlug}}/entries`
* Replace the `Body` section with:

```json
{
  "data": {
    "type": "entry"
  }
}
```
* Replace the contents in the `Tests` section with:
*
```js
const json = pm.response.json()
const d = json.hasOwnProperty("data") ? json.data : json
const entryID = Array.isArray(d) ? d[0].id : d.id
pm.environment.set("entryID", entryID)
```
* Click `Send` to create an empty entry.

>The script in the Tests section of the request, stores the Entry ID returned in a variable called `entryID`.

### Add Product IDs to the Wishlist (Relationship Entry)

With the custom Flow Entry, now you can associate products with a wishlist Entry. Start by creating a new product:

* In Postman, open the `Create a new product` request from `products_legacy` folder
* The `POST` request URL is `{{baseUrl}}/products`
* Replace the contents in the `Body` section with:

```json
{
	"data": {
		"type": "product",
		"name" : "Playtend Switch Controller Pro Slim",
		"slug": "playtend-switch-controller-pro-slim",
		"sku": "PSAL01-GOLD-1",
		"manage_stock": false,
		"description": "Playtend Switch Controller Pro Slim",
		"status" : "live",
		"commodity_type": "physical",
		"price": [
			{ "amount": 7000, "currency": "USD", "includes_tax": true}
		]
	}
}
```
* Replace the contents in the `Tests` section with:
```js
const json = pm.response.json()
const d = json.hasOwnProperty("data") ? json.data : json
const productID = Array.isArray(d) ? d[0].id : d.id
pm.environment.set("productID", productID)
const productSKU = Array.isArray(d) ? d[0].sku : d.sku
pm.environment.set("productSKU", productSKU)
```

* Click `Send`.
* Now to add the new product to the wishlist, open the `Create an entry relationship` request from `flows` folder.
* The `POST` request URL is `{{baseUrl}}/flows/{{flowSlug}}/entries/{{entryID}}/relationships/{{fieldSlug}}`
* Replace the contents in the `Body` section with:

```json
{
    "data": [
        {
            "type": "product",
            "id": "{{productID}}"
        }
    ]
}
```

* Click `Send`

You can repeat these steps to add more products to the wishlist Entry or add several at a time as the `data` is an array field that accepts 1 to many products.
>Use `Get all products` from `products_legacy` folder, to get a list of all the products.

### Get all Wishlists

* In Postman, open the `Get all entries on a flow` request from `flows` folder.
* The `GET` request URL is `{{baseUrl}}/flows/{{flowSlug}}/entries`
* Click `Send`.
>The response must contain the associated products **per Entry** like below.

```json
{
  "data": [
      {
          "id": "58f1dd97-6bc3-4f30-9256-a82defbb9888",
          "type": "entry",
          ...
          ...
          "relationships": {
              "products": {
                  "data": [
                      {
                          "type": "product",
                          "id": "a0226e72-4b74-4927-97be-77bae20cc1db"
                      },
                      {
                          "type": "product",
                          "id": "2de8a081-0690-4511-89be-6c2819422f21"
                      }
                  ]
              }
          }
      }
  ]
}
```

[Next: Extend Customer Resource](./core-resource.md)