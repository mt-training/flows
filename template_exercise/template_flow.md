## 2. Create a Template Flow

In this exercise, you will create a template Flow to hold additional information for your store's gaming products.

### Create a New Template Flow

Complete the following steps to create a custom Flow:

* In Postman, open the `Create a flow` request from `flows` folder.
* Replace the contents in the `Body` section with:

```json
{
    "data": {
    "type": "flow",
    "name": "Gaming Products",
    "slug": "products(gaming)",
    "description": "Attributes for gaming products",
    "enabled": true
    }
}
```

* Click `Send`.
* The script in the Tests section of the request stores the Flow ID in a variable called `flowID`.
* You must then create a new variable in your environment called `flowSlug` and assign it the Flow slug value `products(gaming)`.

[Next: 3. Create Attributes](./template_attributes.md)